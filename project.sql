PRAGMA auto_vacuum = 1;
PRAGMA automatic_index = 1;
PRAGMA case_sensitive_like = 0;
PRAGMA defer_foreign_keys = 0;
PRAGMA encoding = 'UTF-8';
PRAGMA foreign_keys = 1;
PRAGMA ignore_check_constraints = 0;
PRAGMA journal_mode = WAL;
PRAGMA query_only = 0;
PRAGMA recursive_triggers = 1;
PRAGMA reverse_unordered_selects = 0;
PRAGMA secure_delete = 0;
PRAGMA synchronous = NORMAL;

CREATE TABLE kunde (
	id INTEGER PRIMARY KEY,
	email VARCHAR(320) UNIQUE NOT NULL COLLATE NOCASE
	CHECK (email LIKE '%_@_%._%'),
	vorname VARCHAR(100) NOT NULL
	CHECK (vorname NOT GLOB '*[0-9]*' AND vorname <> ''),
	nachname VARCHAR(100) NOT NULL
	CHECK (nachname NOT GLOB '*[0-9]*' AND nachname <> ''),
	passwort VARCHAR(100) NOT NULL
	CHECK (LENGTH(passwort) >= 6),
	adresse_id INTEGER NOT NULL REFERENCES adresse (id)
);

CREATE TABLE adresse (
	id INTEGER PRIMARY KEY,
	plz VARCHAR(5) NOT NULL
	CHECK (plz GLOB '[0-9][0-9][0-9][0-9][0-9]'),
	strasse VARCHAR(30) NOT NULL
	CHECK (strasse NOT GLOB '*[0-9]*' AND strasse <> ''),
	hausnummer INTEGER NOT NULL
	CHECK (hausnummer > 0),
	ort VARCHAR(30) NOT NULL
	CHECK (ort NOT GLOB '*[0-9]*' AND ort <> ''),
	UNIQUE (plz, strasse, hausnummer, ort)
);

CREATE TABLE premiumkunde (
	kunde_id INTEGER PRIMARY KEY REFERENCES kunde (id),
	ablaufdatum DATE NOT NULL DEFAULT (DATE('now', '+6 month'))
	CHECK (STRFTIME('%Y-%m-%d', ablaufdatum) IS NOT NULL),
	hat_ausweis BOOLEAN NOT NULL DEFAULT 0
);

CREATE TABLE angestellter (
	kunde_id INTEGER PRIMARY KEY REFERENCES kunde (id),
	job_bezeichnung VARCHAR(60) NOT NULL,
	gehalt MONEY NOT NULL
	CHECK (gehalt > 0)
);

CREATE TABLE newsletter (
	id INTEGER PRIMARY KEY,
	betreff VARCHAR(30) NOT NULL
	CHECK (betreff <> ''),
	text TEXT NOT NULL
	CHECK (text <> ''),
	datum DATE NOT NULL DEFAULT CURRENT_DATE
	CHECK (STRFTIME('%Y-%m-%d', datum) IS NOT NULL),
	angestellter_id INTEGER NOT NULL REFERENCES angestellter (kunde_id)
);

CREATE TRIGGER updatedate
BEFORE UPDATE OF betreff, text, angestellter_id
ON newsletter
BEGIN
	UPDATE newsletter SET datum = CURRENT_DATE
	WHERE newsletter.id = NEW.id;
END;

CREATE TABLE lieferdienst (
	bezeichnung VARCHAR(60) PRIMARY KEY NOT NULL
	CHECK (bezeichnung <> ''),
	tarif MONEY NOT NULL
	CHECK (tarif >= 0)
);

CREATE TABLE artikel (
	id INTEGER PRIMARY KEY,
	bezeichnung VARCHAR(100) NOT NULL
	CHECK (bezeichnung <> ''),
	beschreibung TEXT NOT NULL
	CHECK (beschreibung <> ''),
	bild BLOB
);

CREATE TABLE schlagwort (
	wort VARCHAR(30) PRIMARY KEY NOT NULL COLLATE NOCASE
	CHECK (wort <> '')
);

CREATE TABLE angebot (
	id INTEGER PRIMARY KEY,
	preis MONEY NOT NULL,
	artikel_id INTEGER NOT NULL REFERENCES artikel (id)
);

CREATE TABLE anbieter (
	bezeichnung VARCHAR(60) PRIMARY KEY NOT NULL
	CHECK (bezeichnung <> '')
);

CREATE TABLE warenkorb (
	id INTEGER PRIMARY KEY,
	name VARCHAR(60) DEFAULT '',
	status VARCHAR(30) NOT NULL
	CHECK (status in ("Bearbeitung", "Versandvorbereitung", "Zustellung", "Zugestellt")),
	kunde_id INTEGER NOT NULL REFERENCES kunde (id)
);

CREATE TABLE lieferabo (
	warenkorb_id INTEGER PRIMARY KEY REFERENCES warenkorb (id) ON DELETE CASCADE,
	beginn DATE NOT NULL
	CHECK (STRFTIME('%Y-%m-%d', beginn) IS NOT NULL),
	ende DATE NOT NULL
	CHECK (STRFTIME('%Y-%m-%d', ende) IS NOT NULL),
	intervall INTEGER
	CHECK (intervall > 0),
	CHECK (ende > beginn)
);

CREATE TABLE liefert (
	warenkorb_id INTEGER PRIMARY KEY REFERENCES warenkorb (id) ON DELETE CASCADE,
	lieferdienst_bezeichnung VARCHAR(60) NOT NULL REFERENCES lieferdienst (bezeichnung),
	bestelldatum DATE NOT NULL DEFAULT CURRENT_DATE
	CHECK (STRFTIME('%Y-%m-%d', bestelldatum) IS NOT NULL),
	lieferdatum DATE
	CHECK (lieferdatum IS NULL OR ((STRFTIME('%Y-%m-%d', lieferdatum) IS NOT NULL) AND lieferdatum >= bestelldatum))
);

CREATE TABLE bietet_an (
	anbieter_bezeichnung VARCHAR(60) NOT NULL REFERENCES anbieter (bezeichnung),
	angebot_id INTEGER NOT NULL REFERENCES angebot (id),
	bestand INTEGER NOT NULL
	CHECK (bestand >= 0),
	PRIMARY KEY (anbieter_bezeichnung, angebot_id)
);

CREATE TABLE inhalt (
	warenkorb_id INTEGER NOT NULL REFERENCES warenkorb (id),
	anbieter_bezeichnung VARCHAR(60) NOT NULL REFERENCES anbieter (bezeichnung),
	angebot_id INTEGER NOT NULL REFERENCES angebot (id),
	anzahl INTEGER NOT NULL
	CHECK (anzahl > 0),
	PRIMARY KEY (warenkorb_id, anbieter_bezeichnung, angebot_id),
	FOREIGN KEY (anbieter_bezeichnung, angebot_id) REFERENCES bietet_an (anbieter_bezeichnung, angebot_id)
);

CREATE TRIGGER warenkorb_inhalt_check BEFORE INSERT ON inhalt
WHEN NOT EXISTS (SELECT * FROM bietet_an AS b
	WHERE NEW.anbieter_bezeichnung = b.anbieter_bezeichnung
	AND NEW.angebot_id = b.angebot_id
	AND NEW.anzahl <= b.bestand)
BEGIN
	SELECT RAISE(FAIL, "Kein passendes Angebot.");
END;

CREATE TRIGGER warenkorb_update_check BEFORE UPDATE ON inhalt
WHEN NOT EXISTS (SELECT * FROM bietet_an AS b
	WHERE NEW.anbieter_bezeichnung = b.anbieter_bezeichnung
	AND NEW.angebot_id = b.angebot_id
	AND NEW.anzahl <= b.bestand)
BEGIN
	SELECT RAISE(FAIL, "Kein passendes Angebot.");
END;

CREATE TABLE empfiehlt (
	artikel_a_id INTEGER NOT NULL REFERENCES artikel (id),
	artikel_b_id INTEGER NOT NULL REFERENCES artikel (id),
	PRIMARY KEY (artikel_a_id, artikel_b_id)
);

CREATE TABLE angemeldet (
	kunde_id INTEGER NOT NULL REFERENCES kunde (id),
	newsletter_id INTEGER NOT NULL REFERENCES newsletter (id) ON DELETE CASCADE,
	PRIMARY KEY (kunde_id, newsletter_id)
);

CREATE TABLE newsletter_enthaelt (
	newsletter_id INTEGER NOT NULL REFERENCES newsletter (id) ON DELETE CASCADE,
	artikel_id INTEGER NOT NULL REFERENCES artikel (id) ON DELETE CASCADE,
	PRIMARY KEY (newsletter_id, artikel_id)
);

CREATE TABLE beschreibt (
	artikel_id INTEGER NOT NULL REFERENCES artikel (id) ON DELETE CASCADE,
	schlagwort_wort VARCHAR(30) NOT NULL REFERENCES schlagwort (wort) COLLATE NOCASE,
	PRIMARY KEY (artikel_id, schlagwort_wort)
);

-- Eintraege

INSERT INTO adresse (plz, strasse, hausnummer, ort, id)
VALUES ('40225', 'Musterstrasse', 37, 'Duesseldorf', 1),
	   ('13057', 'Henricistrasse', 28, 'Berlin', 2),
	   ('23560', 'Defreggerstr.', 12, 'Luebeck', 3),
	   ('90491', 'Grempstrasse', 86, 'Nuernberg', 4),
	   ('50677', 'Rolandstr', 2, 'Köln', 5),
	   ('12345', 'Adminstrasse', 1337, 'Bielefeld', 6);

INSERT INTO kunde (id, email, vorname, nachname, passwort, adresse_id)
VALUES (1, 'max.muster@gmail.com', 'Max', 'Mustermann', 'tintenfisch', 2),
       (2, 'erika.muster@gmail.com', 'Erika', 'Mustermann', 'blumentopf', 2),
	   (3, 'distr103@hhu.de', 'Dietwulf', 'Strobl', 'hunter2', 1),
	   (4, 'tmp@mailinator.com', 'Emmi', 'Hauk', 'passwort', 3),
	   (5, 'hubertapilz@gmx.de', 'Huberta', 'Pilz', '120366', 4),
	   (6, 'oskaranger92@hotmail.com', 'Oskar', 'Anger', 'Gluehbirne', 5),
	   (7, 'sabina.anger@web.de', 'Sabina', 'Anger', '654321', 5),
	   (8, 'admin1@foo.de', 'Simon', 'Slotermeyer', 'qwerty', 6),
	   (9, 'admin2@foo.de', 'Karl', 'Slotermeyer', 'qwerty', 6),
	   (10, 'admin3@foo.de', 'Isabella', 'Slotermeyer', 'qwerty', 6),
	   (11, 'admin4@foo.de', 'David', 'Slotermeyer', 'qwerty', 6),
	   (12, 'admin5@foo.de', 'Anne', 'Slotermeyer', 'qwerty', 6),
	   (13, 'admin6@foo.de', 'Rebecca', 'Slotermeyer', 'qwerty', 6);

INSERT INTO premiumkunde (kunde_id, ablaufdatum, hat_ausweis)
VALUES (3, '2018-05-13', 1),
       (1, '2018-08-02', 0),
	   (2, '2018-09-02', 0);

INSERT INTO angestellter (kunde_id, job_bezeichnung, gehalt)
VALUES (1, 'Systemadministrator', 85000),
       (7, 'CEO', 120000),
	   (5, 'Anwalt', 90000),
	   (8, 'Systemadministrator', 82000),
	   (9, 'Systemadministrator', 68000),
	   (10, 'Systemadministrator', 77000),
	   (11, 'Systemadministrator', 100000),
	   (12, 'Systemadministrator', 76000),
	   (13, 'Systemadministrator', 87654);

INSERT INTO newsletter (id, betreff, text, angestellter_id, datum)
VALUES (1, 'Sonderangebot', 'Wow, so günstig', 1, "2018-02-04"),
	   (2, 'Ausverkauft', 'Nichts mehr da', 7, "2018-01-03");

INSERT INTO lieferdienst (bezeichnung, tarif)
VALUES ('Hermes', 2.3),
	   ('DHL', 5.5);

INSERT INTO artikel (id, bezeichnung, beschreibung, bild)
VALUES (1, 'Hartschalenkoffer', '42000 Liter Volumen, sehr geräumig', NULL),
       (2, 'Lautsprecher', 'Blablablabla', READFILE('res/speaker.png')),
	   (3, 'Schraubenschlüssel', 'Zum schrauben', NULL),
	   (4, 'Pinsel', 'phnglui mglwnafh Cthulhu Rlyeh wgahnagl fhtagn', READFILE('res/brush.png')),
	   (5, 'Kartenspiel', '52 Karten', NULL);

INSERT INTO schlagwort (wort)
VALUES ('Elektronik'),
	   ('Koffer'),
	   ('Musikzubehör'),
	   ('Werkzeug'),
	   ('Malerzubehör');

INSERT INTO angebot (id, preis, artikel_id)
VALUES (1, 42, 1),
       (2, 37.5, 1),
	   (3, 25, 2);

INSERT INTO anbieter (bezeichnung)
VALUES ('amazon'),
       ('technikbilliger'),
	   ('pc-king'),
	   ('happy trolley');

INSERT INTO warenkorb (id, status, kunde_id)
VALUES (1, 'Versandvorbereitung',4),
       (2, 'Zustellung', 6),
	   (3, 'Bearbeitung', 3),
	   (4, 'Bearbeitung', 3);

INSERT INTO lieferabo (warenkorb_id, beginn, ende, intervall)
VALUES (3, '2018-02-01', '2018-05-02', 3),
       (4, '2018-02-02', '2018-12-02', 7);

INSERT INTO liefert (warenkorb_id, lieferdienst_bezeichnung, bestelldatum, lieferdatum)
VALUES (1, 'Hermes', '2017-06-02', '2037-03-15'),
	   (2, 'DHL', '2018-02-11', '2018-02-13');

INSERT INTO bietet_an (anbieter_bezeichnung, angebot_id, bestand)
VALUES ('happy trolley', 1, 20),
       ('amazon', 2, 2000),
	   ('technikbilliger', 3, 500),
	   ('pc-king', 3, 666);

INSERT INTO inhalt (warenkorb_id, anbieter_bezeichnung, angebot_id, anzahl)
VALUES (2, 'amazon', 2, 3),
       (3, 'technikbilliger', 3, 2),
	   (4, 'pc-king', 3, 20);

INSERT INTO empfiehlt (artikel_a_id, artikel_b_id)
VALUES (1,2),
	   (1,3);

INSERT INTO angemeldet (kunde_id, newsletter_id)
VALUES (2, 1),
	   (3, 2);

INSERT INTO newsletter_enthaelt (newsletter_id, artikel_id)
VALUES (1,4), (1,1), (1,3),
	   (2,2), (2,3), (2,4);

INSERT INTO beschreibt (artikel_id, schlagwort_wort)
VALUES (4, 'Werkzeug'),
	   (4, 'Malerzubehör'),
	   (1, 'Koffer'),
	   (2, 'Musikzubehör'),
	   (2, 'Elektronik'),
	   (3, 'Werkzeug');


-- Anfragen

CREATE VIEW lieferdienst_teuer AS
SELECT * FROM lieferdienst
WHERE tarif BETWEEN 5 AND 6;


CREATE VIEW best_bezahlte_admin AS
SELECT * FROM angestellter
WHERE job_bezeichnung = 'Systemadministrator' AND gehalt >= 75000
ORDER BY gehalt DESC
LIMIT 5;


-- Nur zur Hilfe
CREATE VIEW premium_abo_anzahl AS
SELECT p.kunde_id, COUNT(*) AS abo_anzahl
FROM premiumkunde AS p
	JOIN warenkorb AS w
	ON p.kunde_id = w.kunde_id
	JOIN lieferabo as a
	ON w.id = a.warenkorb_id
GROUP BY p.kunde_id;

CREATE VIEW premium_student_max_abos AS
SELECT email FROM premiumkunde
	NATURAL JOIN premium_abo_anzahl
	JOIN kunde ON kunde.id = kunde_id
WHERE hat_ausweis = 1 AND
	abo_anzahl = (
	SELECT MAX(abo_anzahl) FROM premium_abo_anzahl
	);


CREATE VIEW werkzeug_malzub_2empfohlen AS
SELECT * FROM artikel
WHERE id IN (
	SELECT id
	FROM artikel 
	JOIN newsletter_enthaelt 
	ON id = artikel_id
	WHERE EXISTS (
		SELECT * FROM beschreibt
		WHERE schlagwort_wort = 'Werkzeug'
		AND id = artikel_id
		) AND EXISTS (
		SELECT * FROM beschreibt
		WHERE schlagwort_wort = 'Malerzubehör'
		AND id = artikel_id
	)
	GROUP BY id
	HAVING COUNT(*) >= 2
);


CREATE VIEW bild_kein_angebot AS
SELECT * FROM artikel
WHERE NOT EXISTS (
	SELECT * FROM angebot
	WHERE angebot.artikel_id = artikel.id
) AND bild IS NOT NULL
