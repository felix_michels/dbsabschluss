package de.hhu.cs.dbs.internship.project.gui;

import com.alexanderthelen.applicationkit.database.Data;
import de.hhu.cs.dbs.internship.project.Project;
import de.hhu.cs.dbs.internship.project.table.account.Adresse;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AuthenticationViewController extends com.alexanderthelen.applicationkit.gui.AuthenticationViewController {
    protected AuthenticationViewController(String name) {
        super(name);
    }

    public static AuthenticationViewController createWithName(String name) throws IOException {
        AuthenticationViewController viewController = new AuthenticationViewController(name);
        viewController.loadView();
        return viewController;
    }

    @Override
    public void loginUser(Data data) throws SQLException {
        PreparedStatement idStmt = Project.getInstance().getConnection().prepareStatement(
                "SELECT id FROM kunde WHERE email = ? AND passwort = ?");
        idStmt.setString(1, (String)data.get("email"));
        idStmt.setString(2, (String)data.get("password"));
        ResultSet rset = idStmt.executeQuery();
        if (!rset.next()) {
            throw new SQLException("Falsche Email-Adresse oder falsches passwort.");
        }
        login(rset.getInt("id"));
    }

    private void login(int id) throws SQLException {
        Project.getInstance().getData().put("kunde.id", id);
        ResultSet rset;PreparedStatement authStmt = Project.getInstance().getConnection().prepareStatement(
                "SELECT EXISTS(SELECT * FROM angestellter WHERE kunde_id = ?) AS isangestellt"
                        + ", EXISTS(SELECT * FROM premiumkunde WHERE kunde_id = ?) AS ispremium"
        );
        authStmt.setInt(1, id);
        authStmt.setInt(2, id);
        rset = authStmt.executeQuery();
        rset.next();
        Project.getInstance().getData().put("isangestellt", rset.getBoolean("isangestellt"));
        Project.getInstance().getData().put("ispremium", rset.getBoolean("ispremium"));
    }

    @Override
    public void registerUser(Data data) throws SQLException {
        String pw = (String)data.get("password1");
        if (!pw.equals(data.get("password2"))) {
            throw new SQLException("Die Passwoerter stimmen nicht ueberein.");
        }
        Adresse adr = new Adresse
                ( (String)data.get("zipCode")
                , (String)data.get("street")
                , Integer.parseInt((String)data.get("houseNumber"))
                , (String)data.get("city"));

        PreparedStatement insertStmt = Project.getInstance().getConnection().prepareStatement(
                "INSERT INTO kunde (vorname, nachname, email, passwort, adresse_id) VALUES (?, ?, ?, ?, ?)");
        insertStmt.setString(1, (String)data.get("firstName"));
        insertStmt.setString(2, (String)data.get("lastName"));
        insertStmt.setString(3, (String)data.get("eMail"));
        insertStmt.setString(4, pw);
        insertStmt.setInt(5, adr.getAdresseId());
        insertStmt.execute();
    }
}
