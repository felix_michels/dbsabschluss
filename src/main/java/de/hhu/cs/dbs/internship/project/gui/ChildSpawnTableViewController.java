package de.hhu.cs.dbs.internship.project.gui;

import com.alexanderthelen.applicationkit.database.Data;
import com.alexanderthelen.applicationkit.database.Table;
import com.alexanderthelen.applicationkit.gui.Controller;
import com.alexanderthelen.applicationkit.gui.NavigationViewController;
import com.alexanderthelen.applicationkit.gui.SplitViewController;
import com.alexanderthelen.applicationkit.gui.ViewController;
import de.hhu.cs.dbs.internship.project.table.RowClickTable;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ChildSpawnTableViewController extends RowClickTableViewController {

    private String childName = "";
    private TableViewSupplier childControllerSupplier;
    private RowClickTable clickTable;

    public static ChildSpawnTableViewController createWithNameTableAndChildController(
            String name,
            RowClickTable table,
            TableViewSupplier childControllerSupplier)
            throws IOException {
        ChildSpawnTableViewController controller = new ChildSpawnTableViewController(name, table);
        controller.childControllerSupplier = childControllerSupplier;
        controller.clickTable = table;
        controller.loadView();
        return controller;
    }

    protected ChildSpawnTableViewController(String name, Table table) {
        super(name, table);
    }

    @Override
    protected void clickEvent() throws SQLException, IOException {
        Controller parent = getParentController();
        if (parent instanceof NavigationViewController) {
            NavigationViewController navigator = (NavigationViewController)parent;
            navigator.pushViewController(generateChildViewController(getSelectedRow().getData()));
        } else if (parent instanceof SplitViewController) {
            SplitViewController split = (SplitViewController)parent;

            // Remove other siblings
            List<Controller> children = new ArrayList<>(split.getChildControllers());
            children.stream().skip(1)
                    .filter(child -> child instanceof ViewController)
                    .map(child -> (ViewController)child)
                    .forEach(split::removeChildViewControllerFromView);

            split.addChildViewControllerToView(generateChildViewController(getSelectedRow().getData()));
        } else {
            showEditRowViewController();
        }

    }

    private ViewController generateChildViewController(Data data) throws SQLException, IOException {
        ViewController controller = childControllerSupplier.apply(
                getName()+"child",
                clickTable.getTableForRowWithData(data));
        controller.setTitle(childName);
        return controller;
    }

    public void setChildName(String childName) {
        this.childName = childName;
    }
}
