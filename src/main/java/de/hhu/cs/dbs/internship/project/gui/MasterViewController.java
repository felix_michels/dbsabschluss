package de.hhu.cs.dbs.internship.project.gui;

import com.alexanderthelen.applicationkit.database.Table;
import com.alexanderthelen.applicationkit.gui.NavigationViewController;
import com.alexanderthelen.applicationkit.gui.SplitViewController;
import com.alexanderthelen.applicationkit.gui.TableViewController;
import com.alexanderthelen.applicationkit.gui.ViewController;
import de.hhu.cs.dbs.internship.project.Project;
import de.hhu.cs.dbs.internship.project.table.RowClickTable;
import de.hhu.cs.dbs.internship.project.table.account.Account;
import de.hhu.cs.dbs.internship.project.table.artikel.Artikel;
import de.hhu.cs.dbs.internship.project.table.artikel.ArtikelEmpfehlung;
import de.hhu.cs.dbs.internship.project.table.artikel.ArtikelSchlagwort;
import de.hhu.cs.dbs.internship.project.table.dienste.Anbieter;
import de.hhu.cs.dbs.internship.project.table.dienste.Lieferdienst;
import de.hhu.cs.dbs.internship.project.table.lieferabos.Lieferabo;
import de.hhu.cs.dbs.internship.project.table.newsletter.Newsletter;
import de.hhu.cs.dbs.internship.project.table.newsletter.NewsletterVerwaltung;
import de.hhu.cs.dbs.internship.project.table.premium.Premiumkunden;
import de.hhu.cs.dbs.internship.project.table.schlagwoerter.Schlagwoerter;
import de.hhu.cs.dbs.internship.project.table.warenkorb.BestellteWarenkorb;
import de.hhu.cs.dbs.internship.project.table.warenkorb.Bestellungen;
import de.hhu.cs.dbs.internship.project.table.warenkorb.WarenkorbListe;
import javafx.scene.control.TreeItem;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MasterViewController extends com.alexanderthelen.applicationkit.gui.MasterViewController {
    protected MasterViewController(String name) {
        super(name);
    }

    public static MasterViewController createWithName(String name) throws IOException {
        MasterViewController controller = new MasterViewController(name);
        controller.loadView();
        return controller;
    }

    @Override
    protected ArrayList<TreeItem<ViewController>> getTreeItems() {
        return topLevelControllers().stream()
                .map(controller -> {
                    TreeItem<ViewController> treeItem = new TreeItem<>(controller);
                    treeItem.setExpanded(true);
                    return treeItem;
                })
                .collect(Collectors.toCollection(ArrayList::new));
    }

    private static List<ViewController> topLevelControllers() {
        List<ViewController> list = new ArrayList<>();

        try {
            Table account = new Account();
            ViewController accountController = TableViewController.createWithNameAndTable("account", account);
            accountController.setTitle("Account");
            list.add(accountController);

            RowClickTable warenkorbliste = new WarenkorbListe();
            warenkorbliste.setTitle("Warenkorb anklicken, um Inhalt zu sehen");
            ViewController warenkorbController = ChildSpawnTableViewController.createWithNameTableAndChildController(
                    "warenkorbliste",
                    warenkorbliste,
                    TableViewController::createWithNameAndTable);
            NavigationViewController warenkorbTopController = NavigationViewController.createWithName("warenkorb");
            warenkorbTopController.setInitialViewController(warenkorbController);
            warenkorbTopController.setTitle("Warenkorb");
            list.add(warenkorbTopController);

            Table bestellungen = new Bestellungen();
            ViewController bestellungenController = TableViewController.createWithNameAndTable("bestellungen", bestellungen);
            bestellungenController.setTitle("Bestellungen");
            list.add(bestellungenController);

            RowClickTable artikel = new Artikel();
            artikel.setTitle("Artikel anklicken, um Angebote zu sehen");
            ViewController artikelController = ChildSpawnTableViewController.createWithNameTableAndChildController(
                    "artikelliste",
                    artikel,
                    TableViewController::createWithNameAndTable);
            ViewController artikelTopController = SplitViewController.createWithName("artikel");
            artikelTopController.setTitle("Artikel");
            artikelTopController.addChildViewControllerToView(artikelController);
            list.add(artikelTopController);

            RowClickTable artikelSchag = new ArtikelSchlagwort();
            artikelSchag.setTitle("Nach Schlagwörtern mit Komma getrennt suchen");
            ViewController artikelSchlagController = ChildSpawnTableViewController.createWithNameTableAndChildController(
                    "artikelschlag",
                    artikelSchag,
                    TableViewController::createWithNameAndTable);
            ViewController artikelSchlagTopController = SplitViewController.createWithName("artikelSchlagTop");
            artikelSchlagTopController.setTitle("Artikel (Schlagwortsuche)");
            artikelSchlagTopController.addChildViewControllerToView(artikelSchlagController);
            list.add(artikelSchlagTopController);

            Table schlagwoerter = new Schlagwoerter();
            ViewController schlagwortController = TableViewController.createWithNameAndTable("schlagwort", schlagwoerter);
            schlagwortController.setTitle("Schlagwörter");
            list.add(schlagwortController);

            Table anbieter = new Anbieter();
            ViewController anbieterController = TableViewController.createWithNameAndTable("anbieter", anbieter);
            anbieterController.setTitle("Anbieter");
            list.add(anbieterController);

            Table lieferdienst = new Lieferdienst();
            ViewController lieferController = TableViewController.createWithNameAndTable("liefer", lieferdienst);
            lieferController.setTitle("Lieferdienste");
            list.add(lieferController);

            Table newsletter = new Newsletter();
            ViewController newsletterController = TableViewController.createWithNameAndTable("newsletter", newsletter);
            newsletterController.setTitle("Newsletter");
            list.add(newsletterController);

            RowClickTable artikelEmpf = new ArtikelEmpfehlung();
            artikelEmpf.setTitle("Artikel anklicken, um empfohlene Artikel zu sehen");
            ViewController artikelEmpfController = ChildSpawnTableViewController.createWithNameTableAndChildController(
                    "empfehlung",
                    artikelEmpf,
                    TableViewController::createWithNameAndTable);
            ViewController empfohlenListController = SplitViewController.createWithName("empfohlene");
            empfohlenListController.setTitle("Artikel Empfehlungen");
            empfohlenListController.addChildViewControllerToView(artikelEmpfController);
            list.add(empfohlenListController);

            if (Project.isPremium()) {
                Table lieferAbos = new Lieferabo();
                ViewController lieferAboController = TableViewController.createWithNameAndTable("lieferabos", lieferAbos);
                lieferAboController.setTitle("Lieferabos");
                list.add(lieferAboController);
            }


            if (Project.isAngestellt()) {
                Table newsletterVerw = new NewsletterVerwaltung();
                ViewController newsletterVerwController = TableViewController.createWithNameAndTable("newsVer", newsletterVerw);
                newsletterVerwController.setTitle("Newsletter (Verwaltung)");
                list.add(newsletterVerwController);

                Table premiumkunden = new Premiumkunden();
                ViewController premiumController = TableViewController.createWithNameAndTable("premium", premiumkunden);
                premiumController.setTitle("(Premium)kunden");
                list.add(premiumController);

                Table bestellteWar = new BestellteWarenkorb();
                ViewController bestellteVarController = TableViewController.createWithNameAndTable("bestellt", bestellteWar);
                bestellteVarController.setTitle("Bestellte Warenkörbe");
                list.add(bestellteVarController);
            }


        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e2) {
            e2.printStackTrace();
        }
        return list;
    }

}
