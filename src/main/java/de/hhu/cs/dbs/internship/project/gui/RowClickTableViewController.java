package de.hhu.cs.dbs.internship.project.gui;

import com.alexanderthelen.applicationkit.database.Row;
import com.alexanderthelen.applicationkit.database.Table;
import com.alexanderthelen.applicationkit.gui.TableViewController;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableRow;

import java.io.IOException;
import java.sql.SQLException;

public abstract class RowClickTableViewController extends TableViewController {

    protected RowClickTableViewController(String name, Table table) {
        super(name, table);
    }

    private Row selectedRow;

    @Override
    protected void initialize() {
        super.initialize();

        tableView.getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> selectedRow = newValue);

        tableView.setRowFactory(param -> {
            TableRow<Row> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (row == null) {
                    presentAlertDialog(Alert.AlertType.ERROR, "Zeile nicht aktualisiert!", "Es wurde keine Zeile ausgewählt.",
                            null, ButtonType.OK);
                    return;
                }
                if (event.getClickCount() == 2 && !row.isEmpty() && selectedRow == row.getItem()) {
                    try {
                        clickEvent();
                    }catch (Exception e) {
                        presentAlertDialog(Alert.AlertType.ERROR, "Zeile nicht angezeigt!",
                                "Die Zeile kann nicht angezeigt werden.", e, ButtonType.OK);
                    }
                }
            });
            return row;
        });
    }

    protected Row getSelectedRow() {
        return selectedRow;
    }

    protected abstract void clickEvent() throws SQLException, IOException;

}
