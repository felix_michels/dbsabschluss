package de.hhu.cs.dbs.internship.project.gui;

import com.alexanderthelen.applicationkit.database.Table;
import com.alexanderthelen.applicationkit.gui.TableViewController;

import java.io.IOException;

@FunctionalInterface
public interface TableViewSupplier {
    TableViewController apply(String name, Table table) throws IOException;
}
