package de.hhu.cs.dbs.internship.project.table;

import com.alexanderthelen.applicationkit.database.Data;
import com.alexanderthelen.applicationkit.database.Table;

import java.sql.SQLException;

public abstract class RowClickTable extends Table {

    public abstract Table getTableForRowWithData(Data data) throws SQLException;
}
