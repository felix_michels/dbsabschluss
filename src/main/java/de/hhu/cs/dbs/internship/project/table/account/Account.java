package de.hhu.cs.dbs.internship.project.table.account;

import com.alexanderthelen.applicationkit.database.Data;
import com.alexanderthelen.applicationkit.database.Table;
import de.hhu.cs.dbs.internship.project.Project;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Account extends Table {

    private final String hiddenPw = "-----";
    @Override
    public String getSelectQueryForTableWithFilter(String s) throws SQLException {
        return "SELECT id, email, vorname, nachname FROM kunde WHERE id = "
                + Project.getInstance().getData().get("kunde.id");
    }

    @Override
    public String getSelectQueryForRowWithData(Data data) throws SQLException {
        return "SELECT email, vorname, nachname, plz, ort, strasse, hausnummer"
                +", '"+hiddenPw+"' AS passwort, '"+hiddenPw+"' AS passwort2"
                +" FROM kunde JOIN adresse ON adresse_id=adresse.id"
                +" WHERE kunde.id="+data.get("kunde.id");
    }

    @Override
    public void insertRowWithData(Data data) throws SQLException {
        throw new SQLException(getClass().getName() + ".insertRowWithData(Data) nicht implementiert.");
    }

    @Override
    public void updateRowWithData(Data data, Data data1) throws SQLException {
        String passwort = (String)data1.get(".passwort");
        if (!passwort.equals(data1.get(".passwort2"))) {
            throw new SQLException("Passwoerter stimmen nicht ueberein");
        }
        Adresse adr = new Adresse(
                (String)data1.get("adresse.plz"),
                (String)data1.get("adresse.strasse"),
                (int)data1.get("adresse.hausnummer"),
                (String)data1.get("adresse.ort"));

        int kunde_id = (int)Project.getInstance().getData().get("kunde.id");
        Statement adridSelect = Project.getInstance().getConnection().createStatement();
        ResultSet adrIdSet = adridSelect.executeQuery(
                "SELECT adresse_id FROM kunde WHERE id="+kunde_id);
        adrIdSet.next();
        int newAdrId = adr.getUpdatedId(adrIdSet.getInt("adresse_id"));

        PreparedStatement updateStmt = Project.getInstance().getConnection().prepareStatement(
                "UPDATE kunde SET email=?, vorname=?, nachname=?, adresse_id=?"
                + " WHERE id=?");
        updateStmt.setString(1, (String)data1.get("kunde.email"));
        updateStmt.setString(2, (String)data1.get("kunde.vorname"));
        updateStmt.setString(3, (String)data1.get("kunde.nachname"));
        updateStmt.setInt(4, newAdrId);
        updateStmt.setInt(5, kunde_id);
        updateStmt.execute();

        if (!passwort.equals(hiddenPw)) {
            updateStmt = Project.getInstance().getConnection().prepareStatement(
                    "UPDATE kunde SET passwort=? WHERE id=?");
            updateStmt.setString(1, passwort);
            updateStmt.setInt(2, kunde_id);
            updateStmt.execute();
        }
    }

    @Override
    public void deleteRowWithData(Data data) throws SQLException {
        throw new SQLException(getClass().getName() + ".deleteRowWithData(Data) nicht implementiert.");
    }
}
