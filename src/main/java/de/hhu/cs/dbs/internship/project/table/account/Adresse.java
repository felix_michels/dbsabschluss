package de.hhu.cs.dbs.internship.project.table.account;

import de.hhu.cs.dbs.internship.project.Project;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;

public class Adresse {

    public final String plz;
    public final String strasse;
    public final int hausnummer;
    public final String ort;

    public Adresse(String plz, String strasse, int hausnummer, String ort) {
        this.plz = plz;
        this.strasse = strasse;
        this.hausnummer = hausnummer;
        this.ort = ort;
    }

    public int getAdresseId() throws SQLException {
        Optional<Integer> id = getExistingId();
        return id.isPresent() ? id.get() : newAdresseId();
    }

    // Returns empty, if row is updated in place
    public int getUpdatedId(int id) throws SQLException {
        PreparedStatement usecountStmt = Project.getInstance().getConnection().prepareStatement(
                "SELECT COUNT(*) FROM kunde WHERE adresse_id = ?");
        usecountStmt.setInt(1, id);
        ResultSet rset = usecountStmt.executeQuery();
        rset.next();
        int usecount = rset.getInt(1);
        if (usecount > 1) {
            return getAdresseId();
        } else {
            PreparedStatement updateStmt = Project.getInstance().getConnection().prepareStatement(
                    "UPDATE adresse SET plz = ?, strasse = ?, hausnummer = ?, ort = ? WHERE id=?"
            );
            updateStmt.setString(1, plz);
            updateStmt.setString(2, strasse);
            updateStmt.setInt(3, hausnummer);
            updateStmt.setString(4, ort);
            updateStmt.setInt(5,id);
            updateStmt.execute();
            return id;
        }
    }

    private Optional<Integer> getExistingId() throws SQLException {
        PreparedStatement pstmt = Project.getInstance().getConnection().prepareStatement(
                "SELECT id FROM adresse WHERE plz = ? AND strasse = ? AND hausnummer = ? AND ort = ?");
        pstmt.setString(1, plz);
        pstmt.setString(2, strasse);
        pstmt.setInt(3, hausnummer);
        pstmt.setString(4, ort);
        ResultSet rset = pstmt.executeQuery();
        if (rset.next()) {
            return Optional.of(rset.getInt("id"));
        }
        return Optional.empty();
    }

    private int newAdresseId() throws SQLException {
        PreparedStatement pstmt = Project.getInstance().getConnection().prepareStatement(
                "INSERT INTO adresse (plz, strasse, hausnummer, ort) VALUES (?, ?, ?, ?)");
        pstmt.setString(1, plz);
        pstmt.setString(2, strasse);
        pstmt.setInt(3, hausnummer);
        pstmt.setString(4, ort);
        pstmt.execute();

        Statement stmt = Project.getInstance().getConnection().createStatement();
        ResultSet rset = stmt.executeQuery("SELECT last_insert_rowid()");
        rset.next();
        return rset.getInt(1);
    }
}
