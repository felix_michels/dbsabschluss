package de.hhu.cs.dbs.internship.project.table.artikel;

import com.alexanderthelen.applicationkit.database.Data;
import com.alexanderthelen.applicationkit.database.Table;
import de.hhu.cs.dbs.internship.project.Project;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Angebot extends Table {

    private final int artikel_id;

    public Angebot(int artikel_id) {
        this.artikel_id = artikel_id;
    }

    @Override
    public String getSelectQueryForTableWithFilter(String filter) throws SQLException {
        String query = "SELECT angebot.id, preis, anbieter_bezeichnung, bestand"
                + " FROM angebot"
                + " JOIN bietet_an ON bietet_an.angebot_id = angebot.id"
                + " WHERE angebot.artikel_id = " + artikel_id;
        if (filter != null && !filter.isEmpty()) {
            query += " AND anbieter_bezeichnung LIKE '%"+filter+"%'";
        }
        return query;
    }

    @Override
    public String getSelectQueryForRowWithData(Data data) throws SQLException {
        return "SELECT angebot.id, preis, anbieter_bezeichnung, bestand"
                + " FROM angebot"
                + " JOIN bietet_an ON bietet_an.angebot_id = angebot.id"
                + " WHERE angebot.artikel_id = " + artikel_id
                + " AND angebot.id = " + data.get("angebot.id");
    }

    @Override
    public void insertRowWithData(Data data) throws SQLException {
        if (!Project.isAngestellt()) {
            throw new SQLException("Sie sind nicht berechtigt, Angebote hinzuzufügen.");
        }
        Project.getInstance().getConnection().executeUpdate(
                "INSERT INTO angebot (preis, artikel_id) VALUES ('"+data.get("angebot.preis")+"',"+artikel_id+");"
                + " INSERT INTO bietet_an (angebot_id, anbieter_bezeichnung, bestand) VALUES (last_insert_rowid(),'"+data.get("bietet_an.anbieter_bezeichnung")+"',"+data.get("bietet_an.bestand")+")");
    }

    @Override
    public void updateRowWithData(Data oldData, Data newData) throws SQLException {
        throw new SQLException("updateRowWithData ist nicht implementiert.");
    }

    @Override
    public void deleteRowWithData(Data data) throws SQLException {
        PreparedStatement bietetDel = Project.getInstance().getConnection().prepareStatement(
                "DELETE FROM bietet_an WHERE angebot_id=? AND anbieter_bezeichnung=?");
        bietetDel.setInt(1, (Integer) data.get("angebot.id"));
        bietetDel.setString(2, (String) data.get("bietet_an.anbieter_bezeichnung"));
        bietetDel.executeUpdate();

        PreparedStatement angebotDel = Project.getInstance().getConnection().prepareStatement(
                "DELETE FROM angebot WHERE angebot.id=? AND NOT EXISTS "
                + "(SELECT * FROM bietet_an WHERE angebot_id=?)");
        angebotDel.setInt(1, (Integer) data.get("angebot.id"));
        angebotDel.setInt(2, (Integer) data.get("angebot.id"));
        angebotDel.executeUpdate();
    }
}
