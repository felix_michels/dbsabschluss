package de.hhu.cs.dbs.internship.project.table.artikel;

import com.alexanderthelen.applicationkit.database.Data;
import com.alexanderthelen.applicationkit.database.Table;
import de.hhu.cs.dbs.internship.project.Project;
import de.hhu.cs.dbs.internship.project.table.RowClickTable;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Artikel extends RowClickTable {

    @Override
    public Table getTableForRowWithData(Data data) throws SQLException {
        Table table = new Angebot((Integer) data.get("artikel.id"));
        table.setTitle("Angebote für "+data.get("artikel.bezeichnung"));
        return table;
    }

    @Override
    public String getSelectQueryForTableWithFilter(String filter) throws SQLException {
        String query =  "SELECT * FROM artikel";
        if (filter != null && !filter.isEmpty()) {
            query += " WHERE bezeichnung LIKE '%"+filter+"%'"
                    + " OR beschreibung LIKE '%"+filter+"%'";
        }
        return query;
    }

    @Override
    public String getSelectQueryForRowWithData(Data data) throws SQLException {
        return "SELECT id, bezeichnung, beschreibung, bild, GROUP_CONCAT(schlagwort_wort) AS schlagwoerter"
                + " FROM artikel LEFT JOIN beschreibt ON id=artikel_id"
                + " WHERE id = " + data.get("artikel.id")
                + " GROUP BY id";
    }

    @Override
    public void insertRowWithData(Data data) throws SQLException {
        if (!Project.isAngestellt()) throw new SQLException("Sie sind nicht berechtigt, Artikel zu hinzuzufügen.");
        PreparedStatement stmt = Project.getInstance().getConnection().prepareStatement(
                "INSERT INTO artikel (bezeichnung,beschreibung,bild) VALUES (?,?,?)");
        stmt.setString(1, (String) data.get("artikel.bezeichnung"));
        stmt.setString(2, (String) data.get("artikel.beschreibung"));
        stmt.setObject(3,  data.get("artikel.bild"));
        stmt.execute();

        setSchlagwoerter(Project.getInstance().getConnection().executeQuery("SELECT last_insert_rowid()").getInt(1),
                (String) data.get(".schlagwoerter"));
    }

    @Override
    public void updateRowWithData(Data oldData, Data newData) throws SQLException {
        if (!Project.isAngestellt()) throw new SQLException("Sie sind nicht berechtigt, Artikel zu ändern.");
        PreparedStatement stmt = Project.getInstance().getConnection().prepareStatement(
                "UPDATE artikel SET bezeichnung=?, beschreibung=?, bild=? WHERE id=?");
        stmt.setString(1, (String) newData.get("artikel.bezeichnung"));
        stmt.setString(2, (String) newData.get("artikel.beschreibung"));
        stmt.setObject(3, newData.get("artikel.bild"));
        stmt.setInt(4, (Integer) oldData.get("artikel.id"));
        stmt.execute();
        setSchlagwoerter((Integer)oldData.get("artikel.id"), (String)newData.get(".schlagwoerter"));
    }

    @Override
    public void deleteRowWithData(Data data) throws SQLException {
        if (!Project.isAngestellt()) throw new SQLException("Sie sind nicht berechtigt, Artikel zu löschen.");
        PreparedStatement stmt = Project.getInstance().getConnection().prepareStatement(
                "DELETE FROM artikel WHERE id=?");
        stmt.setInt(1, (Integer) data.get("artikel.id"));
        stmt.execute();
    }

    private void setSchlagwoerter(int artikel_id, String schlagwoerter) throws SQLException {
        schlagwoerter = schlagwoerter.replaceAll("\\s+","");
        Project.getInstance().getConnection().executeUpdate(
                "DELETE FROM beschreibt WHERE artikel_id="+artikel_id + "; "
                        + "INSERT OR ROLLBACK INTO beschreibt (artikel_id, schlagwort_wort) VALUES "
                        + Stream.of(schlagwoerter.split(","))
                        .map(s -> "("+artikel_id+", '"+s+"')")
                        .collect(Collectors.joining(",")));
    }

}
