package de.hhu.cs.dbs.internship.project.table.artikel;

import com.alexanderthelen.applicationkit.database.Data;
import com.alexanderthelen.applicationkit.database.Table;
import de.hhu.cs.dbs.internship.project.table.RowClickTable;

import java.sql.SQLException;

public class ArtikelEmpfehlung extends RowClickTable {

    @Override
    public Table getTableForRowWithData(Data data) throws SQLException {
        Table table = new EmpfohlenListe((Integer) data.get("artikel.id"));
        table.setTitle("Von " + data.get("artikel.bezeichnung") + " empfohlene Artikel");
        return table;
    }

    @Override
    public String getSelectQueryForTableWithFilter(String filter) throws SQLException {
        return "SELECT * FROM artikel";
    }

    @Override
    public String getSelectQueryForRowWithData(Data data) throws SQLException {
        return "SELECT * FROM artikel WHERE id="+data.get("artikel.id");
    }

    @Override
    public void insertRowWithData(Data data) throws SQLException {
        throw new SQLException("Artikel können unter 'Artikel' geändert werden.");
    }

    @Override
    public void updateRowWithData(Data oldData, Data newData) throws SQLException {
        throw new SQLException("Artikel können unter 'Artikel' geändert werden.");
    }

    @Override
    public void deleteRowWithData(Data data) throws SQLException {
        throw new SQLException("Artikel können unter 'Artikel' geändert werden.");
    }
}
