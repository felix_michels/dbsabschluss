package de.hhu.cs.dbs.internship.project.table.artikel;

import java.sql.SQLException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ArtikelSchlagwort extends Artikel {

    @Override
    public String getSelectQueryForTableWithFilter(String filter) throws SQLException {
        String query =  "SELECT DISTINCT id, bezeichnung, beschreibung, bild FROM artikel LEFT JOIN beschreibt ON id=artikel_id";
        if (filter != null && !filter.isEmpty()) {
            filter = filter.replaceAll("\\s+","");
            query += " WHERE "
                    + Stream.of(filter.split(","))
                    .map(s -> "schlagwort_wort LIKE '%"+s+"%'")
                    .collect(Collectors.joining(" OR "));
        }
        return query;
    }
}
