package de.hhu.cs.dbs.internship.project.table.artikel;

import com.alexanderthelen.applicationkit.database.Data;
import com.alexanderthelen.applicationkit.database.Table;
import de.hhu.cs.dbs.internship.project.Project;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EmpfohlenListe extends Table {

    private final int artikel_id;

    public EmpfohlenListe(int artikel_id) {
        this.artikel_id = artikel_id;
    }

    @Override
    public String getSelectQueryForTableWithFilter(String filter) throws SQLException {
        return "SELECT id, bezeichnung FROM artikel JOIN empfiehlt ON id=artikel_b_id"
                + " WHERE artikel_a_id = " + artikel_id;
    }

    @Override
    public String getSelectQueryForRowWithData(Data data) throws SQLException {
        return "SELECT id FROM artikel WHERE id="+data.get("artikel.id");
    }

    @Override
    public void insertRowWithData(Data data) throws SQLException {
        if (!Project.isAngestellt()) {
            throw new SQLException("Sie sind nicht berechtigt, Empfehlungen zu ändern.");
        }
        ResultSet countResult = Project.getInstance().getConnection().executeQuery(
                "SELECT COUNT(*) FROM empfiehlt WHERE artikel_a_id="+artikel_id);
        if (countResult.getInt(1) >= 3) {
            throw new SQLException("Ein Artikel kann maximal 3 weitere Artikel empfehlen");
        }
        PreparedStatement stmt = Project.getInstance().getConnection().prepareStatement(
                "INSERT INTO empfiehlt (artikel_a_id, artikel_b_id) VALUES (?,?)");
        stmt.setInt(1, artikel_id);
        stmt.setInt(2, (Integer) data.get("artikel.id"));
        stmt.executeUpdate();
    }

    @Override
    public void updateRowWithData(Data oldData, Data newData) throws SQLException {
        throw new SQLException("Empfehlungen können nur gelöscht oder hinzugefügt werden.");
    }

    @Override
    public void deleteRowWithData(Data data) throws SQLException {
        if (!Project.isAngestellt()) {
            throw new SQLException("Sie sind nicht berechtigt, Empfehlungen zu ändern.");
        }
        PreparedStatement stmt = Project.getInstance().getConnection().prepareStatement(
                "DELETE FROM empfiehlt WHERE artikel_a_id = ? AND artikel_b_id = ?");
        stmt.setInt(1, artikel_id);
        stmt.setInt(2, (Integer) data.get("artikel.id"));
        stmt.executeUpdate();
    }
}
