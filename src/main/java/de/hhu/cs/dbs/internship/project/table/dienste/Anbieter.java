package de.hhu.cs.dbs.internship.project.table.dienste;

import com.alexanderthelen.applicationkit.database.Data;
import com.alexanderthelen.applicationkit.database.Table;
import de.hhu.cs.dbs.internship.project.Project;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Anbieter extends Table {
    @Override
    public String getSelectQueryForTableWithFilter(String filter) throws SQLException {
        String query =  "SELECT * FROM anbieter";
        if (filter != null && !filter.isEmpty()) {
            query += " WHERE bezeichnung LIKE '%"+filter+"%'";
        }
        return query;
    }

    @Override
    public String getSelectQueryForRowWithData(Data data) throws SQLException {
        return "SELECT * FROM anbieter WHERE bezeichnung = '"+data.get("anbieter.bezeichnung")+"'";
    }

    @Override
    public void insertRowWithData(Data data) throws SQLException {
        if (!Project.isAngestellt()) {
            throw new SQLException("Sie sind nicht berechtigt, Anbieter zu ändern.");
        }
        PreparedStatement stmt = Project.getInstance().getConnection().prepareStatement(
                "INSERT INTO anbieter (bezeichnung) VALUES (?)");
        stmt.setString(1, (String) data.get("anbieter.bezeichnung"));
        stmt.executeUpdate();
    }

    @Override
    public void updateRowWithData(Data oldData, Data newData) throws SQLException {
        throw new SQLException("Anbieternamen können nicht gelöscht werden.");
    }

    @Override
    public void deleteRowWithData(Data data) throws SQLException {
        if (!Project.isAngestellt()) {
            throw new SQLException("Sie sind nicht berechtigt, Anbieter zu ändern.");
        }
        PreparedStatement stmt = Project.getInstance().getConnection().prepareStatement(
                "DELETE FROM anbieter WHERE bezeichnung=?");
        stmt.setString(1, (String) data.get("anbieter.bezeichnung"));
        stmt.executeUpdate();
    }
}
