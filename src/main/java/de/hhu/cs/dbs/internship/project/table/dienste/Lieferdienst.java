package de.hhu.cs.dbs.internship.project.table.dienste;

import com.alexanderthelen.applicationkit.database.Data;
import com.alexanderthelen.applicationkit.database.Table;
import de.hhu.cs.dbs.internship.project.Project;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Lieferdienst extends Table {

    @Override
    public String getSelectQueryForTableWithFilter(String filter) throws SQLException {
        String query = "SELECT * FROM lieferdienst";
        if (filter != null && !filter.isEmpty()) {
            query += " WHERE bezeichnung LIKE '%"+filter+"%'";
        }
        return query;
    }

    @Override
    public String getSelectQueryForRowWithData(Data data) throws SQLException {
        return "SELECT * FROM lieferdienst WHERE bezeichnung = '"+data.get("lieferdienst.bezeichnung")+"'";
    }

    @Override
    public void insertRowWithData(Data data) throws SQLException {
        if (!Project.isAngestellt()) {
            throw new SQLException("Sie sind nicht berechtigt, Lieferdienste zu ändern.");
        }
        PreparedStatement stmt = Project.getInstance().getConnection().prepareStatement(
                "INSERT INTO lieferdienst (bezeichnung, tarif) VALUES (?,?)");
        stmt.setString(1, (String) data.get("lieferdienst.bezeichnung"));
        stmt.setObject(2, data.get("lieferdienst.tarif"));
        stmt.executeUpdate();
    }

    @Override
    public void updateRowWithData(Data oldData, Data newData) throws SQLException {
        if (!Project.isAngestellt()) {
            throw new SQLException("Sie sind nicht berechtigt, Lieferdienste zu ändern.");
        }
        String bez = (String) oldData.get("lieferdienst.bezeichnung");
        if (!bez.equals(newData.get("lieferdienst.bezeichnung"))) {
            throw new SQLException("Die Bezeichnung kann nicht geändert werden.");
        }
        PreparedStatement stmt = Project.getInstance().getConnection().prepareStatement(
                "UPDATE lieferdienst SET tarif=? WHERE bezeichnung=?");
        stmt.setObject(1, newData.get("lieferdienst.tarif"));
        stmt.setString(2, bez);
        stmt.executeUpdate();
    }

    @Override
    public void deleteRowWithData(Data data) throws SQLException {
        if (!Project.isAngestellt()) {
            throw new SQLException("Sie sind nicht berechtigt, Lieferdienste zu ändern.");
        }
        PreparedStatement stmt = Project.getInstance().getConnection().prepareStatement(
                "DELETE FROM lieferdienst WHERE bezeichnung = ?");
        stmt.setString(1, (String) data.get("lieferdienst.bezeichnung"));
        stmt.executeUpdate();
    }
}
