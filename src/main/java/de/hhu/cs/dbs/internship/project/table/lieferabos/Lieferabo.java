package de.hhu.cs.dbs.internship.project.table.lieferabos;

import com.alexanderthelen.applicationkit.database.Data;
import com.alexanderthelen.applicationkit.database.Table;
import de.hhu.cs.dbs.internship.project.Project;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Lieferabo extends Table {

    @Override
    public String getSelectQueryForTableWithFilter(String filter) throws SQLException {
        return "SELECT warenkorb_id, beginn, ende, intervall||' Tage' AS intervall FROM lieferabo JOIN warenkorb"
                + " ON id=warenkorb_id"
                + " WHERE kunde_id = " + Project.kunde_id();
    }

    @Override
    public String getSelectQueryForRowWithData(Data data) throws SQLException {
        return "SELECT warenkorb_id, beginn, ende, intervall AS 'intervall (tage)' FROM lieferabo"
                + " WHERE warenkorb_id="+data.get("lieferabo.warenkorb_id");
    }

    @Override
    public void insertRowWithData(Data data) throws SQLException {
        PreparedStatement ownerStmt = Project.getInstance().getConnection().prepareStatement(
                "SELECT kunde_id FROM warenkorb WHERE id=?");
        ownerStmt.setInt(1, (Integer) data.get("lieferabo.warenkorb_id"));
        ResultSet rset = ownerStmt.executeQuery();
        if (!rset.next() || Project.kunde_id() != rset.getInt("kunde_id")) {
            throw new SQLException("Dieser Warenkorb gehört Ihnen nicht.");
        }


        PreparedStatement stmt = Project.getInstance().getConnection().prepareStatement(
                "INSERT INTO lieferabo (warenkorb_id, beginn, ende, intervall) VALUES (?,?,?,?)");
        stmt.setInt(1, (Integer) data.get("lieferabo.warenkorb_id"));
        stmt.setString(2, (String) data.get("lieferabo.beginn"));
        stmt.setString(3, (String) data.get("lieferabo.ende"));
        stmt.setObject(4, data.get("lieferabo.intervall (tage)"));
        stmt.executeUpdate();
    }

    @Override
    public void updateRowWithData(Data oldData, Data newData) throws SQLException {
        if (!oldData.get("lieferabo.warenkorb_id").equals(newData.get("lieferabo.warenkorb_id"))) {
            throw new SQLException("Der warenkorb kann nicht geändert werden.");
        }

        PreparedStatement stmt = Project.getInstance().getConnection().prepareStatement(
                "UPDATE lieferabo SET beginn=?, ende=?, intervall=? WHERE warenkorb_id=?");
        stmt.setString(1, (String) newData.get("lieferabo.beginn"));
        stmt.setString(2, (String) newData.get("lieferabo.ende"));
        stmt.setInt(3, (Integer) newData.get("lieferabo.intervall (tage)"));
        stmt.setInt(4, (Integer) oldData.get("lieferabo.warenkorb_id"));
        stmt.executeUpdate();
    }

    @Override
    public void deleteRowWithData(Data data) throws SQLException {
        PreparedStatement stmt = Project.getInstance().getConnection().prepareStatement(
                "DELETE FROM lieferabo WHERE warenkorb_id=?");
        stmt.setInt(1, (Integer) data.get("lieferabo.warenkorb_id"));
        stmt.executeUpdate();
    }
}
