package de.hhu.cs.dbs.internship.project.table.newsletter;

import com.alexanderthelen.applicationkit.database.Data;
import com.alexanderthelen.applicationkit.database.Table;
import de.hhu.cs.dbs.internship.project.Project;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Newsletter extends Table {

    @Override
    public String getSelectQueryForTableWithFilter(String filter) throws SQLException {
        String query = "SELECT id, betreff, datum"
        + " , EXISTS (SELECT * FROM angemeldet WHERE newsletter_id=id AND kunde_id="+Project.kunde_id()+") AS 'angemeldet?'"
        + " FROM newsletter";
        if (filter != null && !filter.isEmpty()) {
            query += " WHERE betreff LIKE '%"+filter+"%'";
        }
        return query;
    }

    @Override
    public String getSelectQueryForRowWithData(Data data) throws SQLException {
        return "SELECT id, betreff, datum, text"
                + " , "+data.get(".angemeldet?")+" AS 'angemeldet?'"
                +" FROM newsletter WHERE id="+data.get("newsletter.id");
    }

    @Override
    public void insertRowWithData(Data data) throws SQLException {
        throw new SQLException("Sie können keine Newsellter hinzufügen.");
    }

    @Override
    public void updateRowWithData(Data oldData, Data newData) throws SQLException {
        for (String k : oldData.keySet()) {
            if (!k.equals(".angemeldet?") && !oldData.get(k).equals(newData.get(k))) {
                throw new SQLException("Sie können nur Ihren Anmeldestatus ändern.");
            }
        }
        boolean oldAngemeldet = (int)oldData.get(".angemeldet?") != 0;
        boolean newAngemeldet = (int)newData.get(".angemeldet?") != 0;
        if (oldAngemeldet == newAngemeldet) return;
        PreparedStatement stmt;
        if (newAngemeldet) {
            stmt = Project.getInstance().getConnection().prepareStatement(
                    "INSERT INTO angemeldet (kunde_id, newsletter_id) VALUES (?,?)");
        } else {
            stmt = Project.getInstance().getConnection().prepareStatement(
                    "DELETE FROM angemeldet WHERE kunde_id=? AND newsletter_id=?");
        }
        stmt.setInt(1, Project.kunde_id());
        stmt.setInt(2, (Integer) newData.get("newsletter.id"));
        stmt.execute();
    }

    @Override
    public void deleteRowWithData(Data data) throws SQLException {
        throw new SQLException("Sie können keine Newsletter löschen.");
    }
}
