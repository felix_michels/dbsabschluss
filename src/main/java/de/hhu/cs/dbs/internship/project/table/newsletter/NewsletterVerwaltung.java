package de.hhu.cs.dbs.internship.project.table.newsletter;

import com.alexanderthelen.applicationkit.database.Data;
import com.alexanderthelen.applicationkit.database.Table;
import de.hhu.cs.dbs.internship.project.Project;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class NewsletterVerwaltung extends Table {

    @Override
    public String getSelectQueryForTableWithFilter(String filter) throws SQLException {
        String query = "SELECT newsletter.id, betreff, datum, email AS Verfasser"
                + " FROM newsletter JOIN kunde ON newsletter.angestellter_id = kunde.id";
        if (filter != null && !filter.isEmpty()) {
            query += " WHERE betreff LIKE '%"+filter+"%'";
        }
        return query;
    }

    @Override
    public String getSelectQueryForRowWithData(Data data) throws SQLException {
        return "SELECT id, betreff, text FROM newsletter"
                + " WHERE id="+data.get("newsletter.id");
    }

    @Override
    public void insertRowWithData(Data data) throws SQLException {
        PreparedStatement stmt = Project.getInstance().getConnection().prepareStatement(
                "INSERT INTO newsletter (betreff, text, angestellter_id) VALUES (?,?,?)");
        stmt.setString(1, (String) data.get("newsletter.betreff"));
        stmt.setString(2, (String) data.get("newsletter.text"));
        stmt.setInt(3, Project.kunde_id());
        stmt.execute();
    }

    @Override
    public void updateRowWithData(Data oldData, Data newData) throws SQLException {
        PreparedStatement stmt = Project.getInstance().getConnection().prepareStatement(
                "UPDATE newsletter SET betreff=?, text=?, angestellter_id=? WHERE id=?");
        stmt.setString(1, (String) newData.get("newsletter.betreff"));
        stmt.setString(2, (String) newData.get("newsletter.text"));
        stmt.setInt(3, Project.kunde_id());
        stmt.setInt(4, (Integer) oldData.get("newsletter.id"));
        stmt.execute();
    }

    @Override
    public void deleteRowWithData(Data data) throws SQLException {
        PreparedStatement stmt = Project.getInstance().getConnection().prepareStatement(
                "DELETE FROM newsletter WHERE id=?");
        stmt.setInt(1, (Integer) data.get("newsletter.id"));
        stmt.execute();
    }
}
