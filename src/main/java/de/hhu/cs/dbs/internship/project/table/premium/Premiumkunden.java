package de.hhu.cs.dbs.internship.project.table.premium;

import com.alexanderthelen.applicationkit.database.Data;
import com.alexanderthelen.applicationkit.database.Table;
import de.hhu.cs.dbs.internship.project.Project;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Premiumkunden extends Table {

    @Override
    public String getSelectQueryForTableWithFilter(String filter) throws SQLException {
        String query = "SELECT id, email, vorname, nachname, NOT kunde_id IS NULL AS 'premium?'"
                + " FROM kunde LEFT JOIN premiumkunde on id=kunde_id";
        if (filter != null && !filter.isEmpty()) {
            filter = "'%" + filter + "%'";
            query += " WHERE email LIKE " + filter
                    + " OR vorname LIKE " + filter
                    + " OR nachname LIKE " + filter;
        }
        return query;
    }

    @Override
    public String getSelectQueryForRowWithData(Data data) throws SQLException {
        return "SELECT id, email, vorname, nachname, ablaufdatum, " + data.get(".premium?") + " AS 'premium?'"
                + " FROM kunde LEFT JOIN premiumkunde ON id=kunde_id"
                + " WHERE id="+data.get("kunde.id");
    }

    @Override
    public void insertRowWithData(Data data) throws SQLException {
        throw new SQLException("Kunden können hier nicht hinzugefügt werden.");
    }

    @Override
    public void updateRowWithData(Data oldData, Data newData) throws SQLException {
        for (String k : oldData.keySet()) {
            if (!k.equals(".premium?") && !k.equals("premiumkunde.ablaufdatum") && !oldData.get(k).equals(newData.get(k))) {
                throw new SQLException("Sie können nur Premiumkundenstatus und dessen Ablaufdatum ändern.");
            }
        }
        boolean oldPremium = (int)oldData.get(".premium?") != 0;
        boolean newPremium = (int)newData.get(".premium?") != 0;
        String ablaufdatum = (String)newData.get("premiumkunde.ablaufdatum");
        if (ablaufdatum != null && ablaufdatum.isEmpty()) ablaufdatum = null;
        int id = (int)oldData.get("kunde.id");

        if (oldPremium) {
            if (newPremium) {
                PreparedStatement stmt = Project.getInstance().getConnection().prepareStatement(
                        "UPDATE premiumkunde SET ablaufdatum=? WHERE kunde_id=?");
                stmt.setString(1, ablaufdatum);
                stmt.setInt(2, id);
                stmt.execute();
            } else {
                PreparedStatement stmt = Project.getInstance().getConnection().prepareStatement(
                        "DELETE FROM premiumkunde WHERE kunde_id=?");
                stmt.setInt(1,id);
                stmt.execute();
            }
        } else {
            if (newPremium) {
                if (ablaufdatum == null) {
                    PreparedStatement stmt = Project.getInstance().getConnection().prepareStatement(
                            "INSERT INTO premiumkunde (kunde_id) VALUES (?)");
                    stmt.setInt(1,id);
                    stmt.execute();
                } else {
                    PreparedStatement stmt = Project.getInstance().getConnection().prepareStatement(
                            "INSERT INTO premiumkunde (kunde_id, ablaufdatum) VALUES (?,?)");
                    stmt.setInt(1,id);
                    stmt.setString(2, ablaufdatum);
                    stmt.execute();
                }
            }
        }
    }

    @Override
    public void deleteRowWithData(Data data) throws SQLException {
        throw new SQLException("Kunden können hier nicht gelöscht werden.");
    }
}
