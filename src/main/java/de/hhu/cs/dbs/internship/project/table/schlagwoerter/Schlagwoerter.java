package de.hhu.cs.dbs.internship.project.table.schlagwoerter;

import com.alexanderthelen.applicationkit.database.Data;
import com.alexanderthelen.applicationkit.database.Table;
import de.hhu.cs.dbs.internship.project.Project;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Schlagwoerter extends Table {

    @Override
    public String getSelectQueryForTableWithFilter(String s) throws SQLException {
        String query = "SELECT * FROM schlagwort";
        if (s != null && !s.isEmpty()) {
            query += " WHERE wort LIKE '%" + s + "%'";
        }
        return query;
    }

    @Override
    public String getSelectQueryForRowWithData(Data data) throws SQLException {
        return "SELECT * FROM schlagwort WHERE WORT = '" + data.get("schlagwort.wort") + "'";
    }

    @Override
    public void insertRowWithData(Data data) throws SQLException {
        if (Project.isAngestellt()) {
            PreparedStatement pstmt = Project.getInstance().getConnection().prepareStatement(
                    "INSERT INTO schlagwort (wort) VALUES (?)"
            );
            pstmt.setString(1, (String) data.get("schlagwort.wort"));
            pstmt.execute();
        } else {
            throw new SQLException("Sie sind nicht berechtigt, Schlagwoerter hinzuzufuegen.");
        }
    }

    @Override
    public void updateRowWithData(Data data, Data data1) throws SQLException {
        throw new SQLException("Beshtende Schlagwörter können nicht geändert werden.");
    }

    @Override
    public void deleteRowWithData(Data data) throws SQLException {
        if (Project.isAngestellt()) {
            String wort = (String) data.get("schlagwort.wort");
            PreparedStatement pstmt = Project.getInstance().getConnection().prepareStatement(
                    "SELECT EXISTS (SELECT * FROM beschreibt WHERE schlagwort_wort = ?)"
            );
            pstmt.setString(1, wort);
            ResultSet rset = pstmt.executeQuery();
            rset.next();
            boolean exists = rset.getBoolean(1);
            if (!exists) {
                PreparedStatement deleteStmt = Project.getInstance().getConnection().prepareStatement(
                        "DELETE FROM schlagwort WHERE wort = ?"
                );
                deleteStmt.setString(1, wort);
                deleteStmt.execute();
            } else {
                throw new SQLException("Es existieren noch Artikel zu diesem Schlagwort.");
            }
        } else {
            throw new SQLException("Sie sind nicht berechtigt, Schlagwoerter zu loeschen.");
        }
    }
}
