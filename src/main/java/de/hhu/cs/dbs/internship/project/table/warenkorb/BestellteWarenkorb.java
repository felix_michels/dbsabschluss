package de.hhu.cs.dbs.internship.project.table.warenkorb;

import com.alexanderthelen.applicationkit.database.Data;
import com.alexanderthelen.applicationkit.database.Table;

import java.sql.SQLException;

public class BestellteWarenkorb extends Table {

    @Override
    public String getSelectQueryForTableWithFilter(String filter) throws SQLException {
        String query = "SELECT warenkorb.id, email, status"
                + ", CASE WHEN COUNT(angebot_id)=0 THEN 0 ELSE COUNT(*) END AS artikel_anzahl"
                + " FROM warenkorb"
                + " JOIN kunde ON kunde_id = kunde.id"
                + " LEFT JOIN inhalt ON inhalt.warenkorb_id = warenkorb.id"
                + " WHERE NOT warenkorb.status LIKE 'Bearbeitung'";
        if (filter != null && !filter.isEmpty()) {
            query += " AND warenkorb.kunde_email LIKE '%"+filter+"%'";
        }
        query += " GROUP BY warenkorb.id";
        return query;
    }

    @Override
    public String getSelectQueryForRowWithData(Data data) throws SQLException {
        return "SELECT warenkorb.id, email, status, lieferdienst_bezeichnung, bestelldatum, lieferdatum"
                + " FROM warenkorb"
                + " JOIN kunde ON kunde_id = kunde.id"
                + " LEFT JOIN liefert ON warenkorb.id = warenkorb_id"
                + " WHERE warenkorb.id = " + data.get("warenkorb.id");
    }

    @Override
    public void insertRowWithData(Data data) throws SQLException {
        throw new SQLException("Warenkörbe können hier nicht verändert werden.");
    }

    @Override
    public void updateRowWithData(Data oldData, Data newData) throws SQLException {
        throw new SQLException("Warenkörbe können hier nicht verändert werden.");
    }

    @Override
    public void deleteRowWithData(Data data) throws SQLException {
        throw new SQLException("Warenkörbe können hier nicht verändert werden.");
    }
}
