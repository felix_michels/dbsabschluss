package de.hhu.cs.dbs.internship.project.table.warenkorb;

import com.alexanderthelen.applicationkit.database.Data;
import com.alexanderthelen.applicationkit.database.Table;
import de.hhu.cs.dbs.internship.project.Project;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Bestellungen extends Table{

    @Override
    public String getSelectQueryForTableWithFilter(String filter) throws SQLException {
        String query = "SELECT warenkorb.id, name, status, lieferdienst_bezeichnung, bestelldatum, lieferdatum"
                + " FROM warenkorb LEFT JOIN liefert ON warenkorb.id = liefert.warenkorb_id"
                + " WHERE kunde_id="+ Project.kunde_id()
                + " AND warenkorb.status NOT LIKE 'Bearbeitung'";
        if (filter != null && !filter.isEmpty()) {
            query += " AND name LIKE'%" + filter + "%'";
        }
        return query;
    }

    @Override
    public String getSelectQueryForRowWithData(Data data) throws SQLException {
        return "SELECT id, lieferdienst_bezeichnung"
                + " FROM warenkorb LEFT JOIN liefert ON warenkorb.id = liefert.warenkorb_id"
                + " WHERE warenkorb.id="+data.get("warenkorb.id");
    }

    @Override
    public void insertRowWithData(Data data) throws SQLException {
        int warenkorb_id = (int) data.get("warenkorb.id");
        ResultSet rset = Project.getInstance().getConnection().executeQuery("SELECT * FROM warenkorb WHERE id="+warenkorb_id);
        if (!rset.next() || Project.kunde_id() != rset.getInt("kunde_id")) {
            throw new SQLException("Dieser Warenkorb gehört Ihnen nicht.");
        }
        if (!rset.getString("status").equals("Bearbeitung")) {
            throw new SQLException("Dieser Warenkorb ist bereits bestellt");
        }
        PreparedStatement stmt = Project.getInstance().getConnection().prepareStatement(
                "INSERT INTO liefert (warenkorb_id, lieferdienst_bezeichnung) VALUES (?,?)");
        stmt.setInt(1, warenkorb_id);
        stmt.setString(2, (String) data.get("liefert.lieferdienst_bezeichnung"));
        stmt.executeUpdate();

        stmt = Project.getInstance().getConnection().prepareStatement(
                "UPDATE warenkorb SET status = 'Versandvorbereitung' WHERE warenkorb.id=?");
        stmt.setInt(1, warenkorb_id);
        stmt.executeUpdate();
    }

    @Override
    public void updateRowWithData(Data oldData, Data newData) throws SQLException {
        throw new SQLException("Sie können bestellte Warenkörbe nicht stornieren oder ändern.");
    }

    @Override
    public void deleteRowWithData(Data data) throws SQLException {
        throw new SQLException("Sie können bestellte Warenkörbe nicht stornieren oder ändern.");
    }
}
