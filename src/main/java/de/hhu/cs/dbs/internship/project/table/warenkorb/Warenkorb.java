package de.hhu.cs.dbs.internship.project.table.warenkorb;

import com.alexanderthelen.applicationkit.database.Data;
import com.alexanderthelen.applicationkit.database.Table;
import de.hhu.cs.dbs.internship.project.Project;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Warenkorb extends Table {

    private final int id;

    public Warenkorb(int id) {
        this.id = id;
    }

    @Override
    public String getSelectQueryForTableWithFilter(String filter) throws SQLException {
        String query = "SELECT inhalt.angebot_id, artikel.bezeichnung AS artikel, angebot.preis, inhalt.anbieter_bezeichnung, inhalt.anzahl"
                + " FROM inhalt"
                + " JOIN angebot ON angebot.id = inhalt.angebot_id"
                + " JOIN anbieter ON anbieter.bezeichnung = inhalt.anbieter_bezeichnung"
                + " JOIN artikel ON artikel.id = angebot.artikel_id"
                + " JOIN warenkorb ON warenkorb.id = inhalt.warenkorb_id"
                + " WHERE warenkorb.id = " + id;
        if (filter != null && !filter.isEmpty()) {
            query += " AND artikel.bezeichnung LIKE '%"+filter+"%'";
        }
        return query;
    }

    @Override
    public String getSelectQueryForRowWithData(Data data) throws SQLException {
        return "SELECT angebot_id, anbieter_bezeichnung, anzahl"
                + " FROM inhalt"
                + " WHERE angebot_id = " + data.get("inhalt.angebot_id")
                + " AND anbieter_bezeichnung = '" + data.get("inhalt.anbieter_bezeichnung") + "'"
                + " AND anzahl = "+ data.get("inhalt.anzahl");
    }

    @Override
    public void insertRowWithData(Data data) throws SQLException {
        PreparedStatement stmt = Project.getInstance().getConnection().prepareStatement(
                "INSERT INTO inhalt (warenkorb_id, anbieter_bezeichnung, angebot_id, anzahl) VALUES (?,?,?,?)");
        stmt.setInt(1, id);
        stmt.setString(2, (String) data.get("inhalt.anbieter_bezeichnung"));
        stmt.setInt(3, (Integer) data.get("inhalt.angebot_id"));
        stmt.setInt(4, (Integer) data.get("inhalt.anzahl"));
        stmt.executeUpdate();
    }

    @Override
    public void updateRowWithData(Data oldData, Data newData) throws SQLException {
        for (String k : oldData.keySet()) {
            if (!k.equals("inhalt.anzahl") && !oldData.get(k).equals(newData.get(k))) {
                throw new SQLException("Sie können hier nur die Anzahl ändern.");
            }
        }
        PreparedStatement stmt = Project.getInstance().getConnection().prepareStatement(
                "UPDATE inhalt SET anzahl = ? WHERE anbieter_bezeichnung=? AND angebot_id=? AND warenkorb_id=?");
        stmt.setInt(1, (Integer) newData.get("inhalt.anzahl"));
        stmt.setString(2, (String) oldData.get("inhalt.anbieter_bezeichnung"));
        stmt.setInt(3, (Integer) oldData.get("inhalt.angebot_id"));
        stmt.setInt(4, id);
        stmt.executeUpdate();
    }

    @Override
    public void deleteRowWithData(Data data) throws SQLException {
        if (!inBearbeitung()) {
            throw new SQLException("Angebote können nicht nach Bestellung aus dem Warenkorb entfernt werden.");
        }
        Project.getInstance().getConnection().executeUpdate(
                "DELETE FROM inhalt WHERE angebot_id="+data.get("angebot.id")
                        + " AND anbieter_bezeichnung ='" + data.get("anbieter.bezeichnung") + "'"
                        + " AND warenkorb_id = " + id);
    }

    private boolean inBearbeitung() throws SQLException {
        ResultSet res = Project.getInstance().getConnection().executeQuery(
                "SELECT status FROM warenkorb WHERE id="+id);
        return res.getString(1).equals("Bearbeitung");
    }
}
