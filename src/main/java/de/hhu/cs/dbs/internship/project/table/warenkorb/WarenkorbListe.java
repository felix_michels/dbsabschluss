package de.hhu.cs.dbs.internship.project.table.warenkorb;

import com.alexanderthelen.applicationkit.database.Data;
import com.alexanderthelen.applicationkit.database.Table;
import de.hhu.cs.dbs.internship.project.Project;
import de.hhu.cs.dbs.internship.project.table.RowClickTable;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class WarenkorbListe extends RowClickTable {

    @Override
    public String getSelectQueryForTableWithFilter(String filter) throws SQLException {
        String query = "SELECT id, name, status"
                + ", CASE WHEN COUNT(angebot_id)=0 THEN 0 ELSE COUNT(*) END AS artikel_anzahl"
                + " FROM warenkorb"
                + " LEFT JOIN inhalt ON inhalt.warenkorb_id = warenkorb.id"
                + " WHERE warenkorb.kunde_id = " + Project.getInstance().getData().get("kunde.id");
        if (filter != null && !filter.isEmpty()) {
            query += " AND warenkorb.name LIKE '%"+filter+"%'";
        }
        query += " GROUP BY warenkorb.id";
        return query;
    }

    @Override
    public String getSelectQueryForRowWithData(Data data) throws SQLException {
        return "SELECT id, name FROM warenkorb WHERE id = " + data.get("warenkorb.id");
    }

    @Override
    public void insertRowWithData(Data data) throws SQLException {
        PreparedStatement stmt = Project.getInstance().getConnection().prepareStatement(
                "INSERT INTO warenkorb (name, kunde_id, status) VALUES (?,?,?)");
        stmt.setString(1, (String) data.get("warenkorb.name"));
        stmt.setInt(2, (Integer) Project.getInstance().getData().get("kunde.id"));
        stmt.setString(3, "Bearbeitung");
        stmt.execute();
    }

    @Override
    public void updateRowWithData(Data oldData, Data newData) throws SQLException {
        PreparedStatement stmt = Project.getInstance().getConnection().prepareStatement(
                "UPDATE warenkorb SET name = ? WHERE id = ?");
        stmt.setString(1, (String) newData.get("warenkorb.name"));
        stmt.setInt(2, (Integer) newData.get("warenkorb.id"));
        stmt.execute();
    }

    @Override
    public void deleteRowWithData(Data data) throws SQLException {
        // TODO decide, when to allow deletion
        PreparedStatement stmt = Project.getInstance().getConnection().prepareStatement(
                "DELETE FROM warenkorb WHERE id = ?");
        stmt.setInt(1, (Integer) data.get("warenkorb.id"));
        stmt.execute();
    }

    @Override
    public Table getTableForRowWithData(Data data) throws SQLException {
        Table table = new Warenkorb((Integer) data.get("warenkorb.id"));
        String name = (String) data.get("warenkorb.name");
        if (name == null || name.isEmpty()) {
            name = "Warenkorb " + data.get("warenkorb.id");
        }
        table.setTitle(name);
        return table;
    }
}
